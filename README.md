# LiveBus

#### 介绍
可以取代  EventBus RxBus 因为基于LiveData 所以可以感知 宿主的生命周期
对于观察者只管 注册，销毁我们会根据宿主 在销毁的时候触发销毁
并且 在视图非显示状态下并不发出更新通知

#### 起因
LiveData 本身是事件粘性的，根据大神们的分析，粘性与否可以根据 观察者 version 来决定
网络上的大神们，很早就使用了hook 方案 实现了，所以鄙人想寻找一个不实用hook的方案。于是找到
https://github.com/mrme2014/hi_jetpack
但是这里面存在一个问题，作为事件的发送方 需要知道事件是否是粘性的
1.个人认为事件是否是粘性，发送方不需要注意，应该是观察者来决定
2.与日常我们使用liveData 有所差别

#### 思路
看了上面的源码，在结合自己的思考
参考 LiveData 中的 wrapper 部分我们重新自己重新包装一层。
然后将上层传递过来的观察者缓存住，将我们自己的观察者交付给liveData去执行。
LiveData还是执行LiveData内的一套。等到触发onChange的时候，这时候我们就可以根据自己控制的版本号来进行数据分发
至于粘性判断，这个在我们直接观察者一开始初始化的时候就进行判断，因为不需要粘性的话，只要当前观察者版本号等于
当前数据版本号即可