package com.liuyc.test.app

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.liuyc.test.app.event.NormalEvent
import com.liuyc.test.app.event.StickyEvent
import com.liuyc.test.app.ext.click
import com.liuyc.test.app.utils.LiveDataBus
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initEvent();
        registerLiveData();
    }

    fun registerLiveData(){

        LiveDataBus.with<String>(NormalEvent::class.java).observe(this, androidx.lifecycle.Observer {
            Toast.makeText(this@MainActivity, "Main页面的非粘性注册，触发${it}", Toast.LENGTH_SHORT).show()
        })
    }

    fun initEvent(){

        mOpenViewBtn.click {
            startActivity(Intent(this@MainActivity, TestActivity::class.java))
        }

        mSendThisMsgBtn.click {
            LiveDataBus.with<String>(NormalEvent::class.java).setValue(" 非粘性事件 ")
        }

        mSendExternalMsgBtn.click {
            LiveDataBus.with<String>(StickyEvent::class.java).setValue(" 粘性事件 ")
        }
    }

}