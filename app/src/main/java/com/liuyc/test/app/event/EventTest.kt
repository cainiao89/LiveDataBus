package com.liuyc.test.app.event

/**
 * Event
 * 非粘性事件
 * 刘隽
 */
class NormalEvent

/**
 * StickyEvent
 *  粘性事件
 *
 * 刘隽
 */
class StickyEvent