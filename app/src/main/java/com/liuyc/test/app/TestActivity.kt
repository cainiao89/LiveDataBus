package com.liuyc.test.app

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.liuyc.test.app.event.NormalEvent
import com.liuyc.test.app.event.StickyEvent
import com.liuyc.test.app.ext.click
import com.liuyc.test.app.utils.LiveDataBus
import kotlinx.android.synthetic.main.activity_main.*

class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        registerLiveData();
    }

    fun registerLiveData(){

        LiveDataBus.with<String>(StickyEvent::class.java).observerSticky(this, true, Observer {
            Toast.makeText(this@TestActivity, "Test 页面的粘性注册 触发${it}", Toast.LENGTH_SHORT).show()
        })

        LiveDataBus.with<String>(StickyEvent::class.java).observe(this, Observer {
            Toast.makeText(this@TestActivity, "Test 页面的非粘性注册 触发${it}", Toast.LENGTH_SHORT).show()
        })


    }

}